1. You can quickly choose a name by using RandomNamePicker.org, completely random.

2. All of these names can be edited, deleted and saved, you can enter 10 names, 20 names even 100 names, in particular the save function, you can once for all just set your password.

3. Almost all of the names you enter can find the meaning of the name, which makes you better teaching in your class, if you are a teacher.

4. Not a spin wheel, not a fruit machine, but you can choose the name more convenient, and more easily.

5. This tool have music, full of fun, make your class more interesting.

6. Please don't hesitate to contact us with any questions.